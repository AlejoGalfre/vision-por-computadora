import cv2  #Importamos la libreria para leer y escribir el video

cap = cv2.VideoCapture(0)   #Abrimos la cámara 0 y la asociamos con la variable cap
fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')  #Generamos el codigo de 4 caracteres para escribir el video

W = cap.get(cv2.CAP_PROP_FRAME_WIDTH)   #Obtenemos el ancho de la imagen
H = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  #Obtenemos el alto de la imagen

out = cv2.VideoWriter('output.avi', fourcc, 20, (int(W), int(H)))   #Creamos un archivo de video "output.avi" con la codificacion y tamaño obtenidos anteriormente, y con 20 fps

while(cap.isOpened()):                  #Mientras la cámara esté abierta
    ret, frame = cap.read()             #Leemos 1 frame (imagen)
    if ret is True:                     #Si efectivamente lee algo
        out.write(frame)                #Escribimos la imagen leida en el archivo de salida
        cv2.imshow('frame', frame)      #Mostramos la imagen leida en pantalla
        if cv2.waitKey(1) & 0xFF == ord('q'):   #Esperamos 1 ms a que se presione una tecla
            break                   #Si la letra presionada es una q (mayúscula o minúscula) salimos del bucle
    else:
        break       #Si no lee nada de la camara salimos del bucle

cap.release()               #Cerramos la cámara
out.release()               #Cerramos el archivo de salida
cv2.destroyAllWindows()     #Cerramos todas las ventanas abiertas
