import sys  #Importamos la librería para poder leer el vector de argumentos
import cv2 #Importamos la libreria para capturar y mostrar el video

if(len(sys.argv) > 1):   #Si llaman al programa con algún argumento
    filename = sys.argv[1]  #Guardamos el nombre del video en filename
else:   #Si no, informamos como se usa el programa y salimos con 0
    print('Pass a filename as first argument')
    sys.exit(0)

cap = cv2.VideoCapture(filename)    #Abrimos el archivo de video y lo guardamos en cap

fps = cap.get(cv2.CAP_PROP_FPS)     #Obtenemos las imagenes por segundo (fps) del video. Es analoga a una frecuencia
delay = 1/fps   #Invertimos los fps para obtener el periodo de tiempo entre frames
delay *= 1000   #Lo multiplicamos por mil para que este expresado en ms

while(cap.isOpened()):  #Mientras no lleguemos al final del archivo
    ret, frame = cap.read() #Leemos un frame del archivo (imagen)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  #Pasamos el frame a escala de grises

    cv2.imshow('frame', gray)   #Lo mostramos en pantalla en una ventana llamada "frame"
    if((cv2.waitKey(int(delay)) & 0xFF) == ord('q')):   #Pasamos el delay a entero y esperamos durante ese tiempo a que se presione una tecla
        break   #Si es una "q" (mayúscula o minúscula) salimos del bucle.

cap.release()   #Cerramos el archivo
cv2.destroyAllWindows()     #Cerramos todas las ventanas
