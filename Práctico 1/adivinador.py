import random                   #Importamos libreria


def adivinar(intentos):
    numero = random.randint(0, 100)     #Generamos un numero ENTERO aleatorio y lo guardamos en numero
    cuenta = 0  #Variable para almacenar la cantidad de intentos REALIZADOS

    while intentos > 0:
        guess = int(input("\nIngrese un numero entero: "))

        intentos -= 1
        cuenta += 1

        if guess == numero:
            print("Felicitaciones! Ha adivinado el numero en ", cuenta, "intentos.")
            return 0

        elif guess > numero:
            print("Lo siento, el numero es menor.")

        else:
            print("Lo siento, el numero es mayor.")

        if intentos > 0:
            print("Intente nuevamente (" + str(intentos) + " intentos restantes).")
        else:
            print("Se ha quedado sin intentos. El numero generado era " + str(numero) + ". Mejor suerte para la próxima!")
            return 1


print("Bienvenido al adivinador de números!")
print("Se generará un número aleatorio comprendido entre 0 y 100, y usted debe intentar adivinar cúal es.")
intentos = int(input("Cuantas veces intentará adivinar? "))

adivinar(intentos)
