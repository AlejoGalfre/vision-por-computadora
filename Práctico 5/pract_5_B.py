import cv2              # Importamos la libreria para leer y escribir imágenes, y detectar los eventos del mouse
import numpy as np      # Importamos la libreria para crear y copiar matrices y la renombramos

drawing = False         # Esta variable indica que se está dibujando el rectangulo. Se pone en True cuando se apreta el mouse y en False cuando se suelta
waiting = False         # Esta variable indica que ya se seleccionó parte de la imagen y se está esperando a que se presione una tecla. Se pone en true al terminar de dibujar el rectangulo y en False al guardar o resetear la selección

xi, yi = -1, -1         # Coordenadas X,Y iniciales del rectángulo
xf, yf = -1, -1         # Coordenadas X,Y finales del rectángulo


def seleccionar(event, x, y, flags, param):         # Función callback para cuando se detecta un evento del mouse
    global drawing, waiting, xi, yi, xf, yf, img, img1  # Declaramos las variables globales para que no cree nuevas
    if waiting is False:        # Este if evita que se dibuje un nuevo rectangulo sin haber guardado o reseteado el anterior
        if event == cv2.EVENT_LBUTTONDOWN:      # Si se presiona el boton izquierdo
            drawing = True                      # Se activa el modo dibujo
            xi, yi = x, y                       # Y se guardan las coordenadas iniciales
        elif event == cv2.EVENT_MOUSEMOVE:      # Si se mueve el mouse
            if drawing is True:                 # Y se está dibujando
                img1 = np.copy(img)             # Carga nuevamente la imagen original (para que no queden todos los rectangulos anteriores)
                cv2.rectangle(img1, (xi, yi), (x, y), (0, 0, 255), 1)   # Y dibuja un rectangulo desde x,y inicial hasta el punto actual
        elif event == cv2.EVENT_LBUTTONUP:      # Si se suelta el boton del mouse
            drawing = False         # Salimos del modo dibujo
            waiting = True          # Entramos en el modo de espera
            xf, yf = x, y           # Guardamos las coordenadas finales


def transf_euclid(angle = 0, tx = 0, ty = 0):   #Rotación y traslacion a aplicar ; valores por defecto 0
    global xi, yi, xf, yf, waiting      # Declaramos las variables globales para que no cree nuevas

    waiting = False     # Salimos del modo de espera

    if xi > xf:         # En esta porción de codigo se verifica que las coordenadas finales sean mayores que las iniciales
        aux = xi        # En caso contrario, se intercambian
        xi = xf         # Se hace por las dudas comiencen a dibujar el rectángulo desde abajo o desde la derecha
        xf = aux
    if yi > yf:
        aux = yi
        yi = yf
        yf = aux

    rec = np.zeros((yf-yi, xf-xi, 3), np.uint8)     # Creamos una matriz del tamaño del rectángulo seleccionado. Primero filas (y), despues columnas (x) y de profundidad 3 porque es a color

    for i in range(yf-yi):      # Iteramos la cantidad de filas que tenga el rectángulo
        for j in range(xf-xi):  # Iteramos la cantidad de columnas que tenga el rectángulo
            rec[i][j] = img[yi+i][xi+j]     # Guardamos cada pixel de la porción seleccionada en la matriz creada anteriormente

    M = np.float32([ [np.cos(angle), np.sin(angle), tx], [-np.sin(angle), np.cos(angle), ty] ])  #Calculamos parte de la matriz H

    (h, w, d) = rec.shape   #Obtenemos el tamaño

    img_transf = cv2.warpAffine(rec, M, (w, h))     #Aplicamos la transformacion

    cv2.imwrite("Recorte_Transformado.jpg", img_transf)     # Guardamos esta nueva imagen como Recorte_Transformado.jpg


img = cv2.imread("hojas.jpg")       # Leemos la imagen a recortar
img1 = np.copy(img)         # Creamos una segunda imagen que es la que vamos a manipular. Esto sirve para cargar la original cada vez que vayamos a dibujar y que no queden los dibujos anteriores
cv2.namedWindow('Image')    # Creamos una ventana llamada image
cv2.setMouseCallback('Image', seleccionar)  # Definimos que si se produce un evento de mouse sobre esa ventana se debe llamar a la función seleccionar

tx = 50    #Traslacion en x
ty = 50    #Traslacion en y
angle = np.pi/4     #Ángulo de rotación

while(1):
    cv2.imshow('Image', img1)   # Mostramos la imagen en pantalla
    k = cv2.waitKey(1) & 0xFF   # Esperamos 1 ms a que se presione una tecla
    if k == ord('e') and waiting is True:   # Si la tecla que se presionó es una e, y ya selecionamos una porcion de la imagen
        transf_euclid(angle, tx, ty)   # Aplicamos la transformacion
        break       # Y salimos
    elif k == ord('r'):         # Si la tecla que se presionó es una r
        img1 = np.copy(img)     # Cargamos nuevamente la imagen original
        waiting = False         # Y salimos del modo de espera para poder selecionar nuevamente
    elif k == ord('q'):         # Si la tecla que se presionó es una q
        break                   # Salimos

cv2.destroyAllWindows()         # Cerramos todas las ventanas abiertas
