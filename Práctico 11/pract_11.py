import numpy as np
import cv2

MIN_MATCH_COUNT = 10

path1 = "img1.jpg"
path2 = "img2.jpg"

img1 = cv2.imread(path1)    # Leemos la imagen 1
img2 = cv2.imread(path2)    # Leemos la imagen 2

(h1, w1, d1) = img1.shape   #Obtenemos el tamaño de la imagen 1
(h2, w2, d2) = img2.shape   #Obtenemos el tamaño de la imagen 2

#Las imagenes originales son de 9248 x 6936, asique les reducimos el tamaño
escala = 10/100     #Escala = 10%

w1_reduc = int(w1*escala)   #Calculamos el nuevo ancho, lo redondeamos a un numero entero de pixeles y lo guardamos
h1_reduc = int(h1*escala)   #Calculamos el nuevo alto, lo redondeamos a un numero entero de pixeles y lo guardamos
w2_reduc = int(w2*escala)
h2_reduc = int(h2*escala)

img1_reduc = cv2.resize(img1, (w1_reduc, h1_reduc), interpolation = cv2.INTER_AREA) #Reducimos el tamaño conservando la relacion de aspecto
img2_reduc = cv2.resize(img2, (w2_reduc, h2_reduc), interpolation = cv2.INTER_AREA) #Reducimos el tamaño conservando la relacion de aspecto

img1_gris = cv2.cvtColor(img1_reduc, cv2.COLOR_BGR2GRAY)    #Pasamos a escala de grises la imagen 1
img2_gris = cv2.cvtColor(img2_reduc, cv2.COLOR_BGR2GRAY)    #Pasamos a escala de grises la imagen 2

#img1_gris = np.float32(img1_gris)       #Modificamos el tipo de datos de los pixeles
#img2_gris = np.float32(img2_gris)       #Modificamos el tipo de datos de los pixeles

dscr = cv2.SIFT_create()    #Inicializamos el detector y el descriptor

kp1, des1 = dscr.detectAndCompute(img1_gris, None)  #Encontramos los puntos clave y los descriptores con SIFT en la imagen 1
kp2, des2 = dscr.detectAndCompute(img2_gris, None)  #Encontramos los puntos clave y los descriptores con SIFT en la imagen 2

matcher = cv2.BFMatcher(cv2.NORM_L2)                #Iniciamos el matcher basado en la norma L2 (distancia)

matches = matcher.knnMatch(des1, des2, k = 2)       #Le pasamos al matcher los descriptores de las caracteristicas detectadas con SIFT
#El K=2 es para que asocie cada caracteristica de la imagen 1 con las 2 caracteristicas mas similares (menor distancia) de la imagen 2

# Guardamos los buenos matches usando el test de razón de Lowe
good = []

for m, n in matches:                            #Revisamos las dos coincidencias (matches) de cada una de las caracteristicas
    if m.distance < 0.7 * n.distance:           #Si la mejor coincidencia m (mas cercana/menor distancia) se encuentra a una distancia menor al 70% de la distancia con la segunda coincidencia
        good.append(m)                          #La guardamos en la lista. Caso contrario, no podemos asegurar con cual se corresponde (porque la diferencia es muy pequeña), por lo que se descartan esos matches

if ( len(good) > MIN_MATCH_COUNT ):           #Si la cantidad de puntos correspondientes es mayor al umbral definido
    src_pts = np.float32( [kp1[m.queryIdx].pt for m in good] ).reshape(-1, 1, 2)    #Guardamos la ubicacion (coordenadas X e Y) de los puntos caracteristicos de la primera imagen
    dst_pts = np.float32( [kp2[m.trainIdx].pt for m in good] ).reshape(-1, 1, 2)    #Guardamos la ubicacion (coordenadas X e Y) de los puntos caracteristicos de la segunda imagen

    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0)     #Computamos la homografía con RANSAC (para eliminar los outliers mediante el consenso)

    wimg2 = cv2.warpPerspective(img2_reduc, H, (2*w2_reduc, h2_reduc)) #Aplicamos la transformación perspectiva H a la imagen 2 (hacemos que la imagen resultante tenga el doble de ancho para que entre todo el cuadro)

    # Mezclamos ambas imágenes
    alpha = 0.5     #Modificamos las intensidades de cada pixel de las imagenes por un factor alpha de 0.5

    blend = np.zeros((h1_reduc, 2*w1_reduc, 3), dtype = np.uint8)

    for i in range(h1_reduc):
        for j in range(2*w1_reduc):
            if(j >= w1_reduc):
                blend[i][j] = wimg2[i][j] * alpha
            else:
                blend[i][j] = wimg2[i][j] * alpha + img1_reduc[i][j] * (1 - alpha)

    #blend = np.array(wimg2 * alpha + img1_reduc * (1 - alpha), dtype = np.uint8)  #De esta manera, en las zonas de solapamiento, se tendra el brillo original, y en las zonas aportadas por una sola imagen, apareceran mas oscuras

    cv2.imwrite('img1_reduc.png', img1_reduc)
    cv2.imwrite('img2_reduc.png', img2_reduc)
    cv2.imwrite('img2_trans.png', wimg2)
    cv2.imwrite('img_result.png', blend)

    cv2.imshow('Imagen resultante', blend)
    cv2.waitKey()

else:
    print("No se han encontrado suficientes coincidencias entre las imagenes.")
