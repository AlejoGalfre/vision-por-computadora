import cv2              # Importamos la libreria para leer y escribir imágenes, y detectar los eventos del mouse
import numpy as np      # Importamos la libreria para crear y copiar matrices y la renombramos

drawing = False         # Esta variable indica que se está dibujando el rectangulo. Se pone en True cuando se apreta el mouse y en False cuando se suelta
waiting = False         # Esta variable indica que ya se seleccionó parte de la imagen y se está esperando a que se presione una tecla. Se pone en true al terminar de dibujar el rectangulo y en False al guardar o resetear la selección
selecting = False       # Esta variable indica que se están seleccionando los 3 puntos para la transformacion afin. Se pone en True luego de presionar la letra a y en False luego de elegir 3 puntos validos

xi, yi = -1, -1         # Coordenadas X,Y iniciales del rectángulo
xf, yf = -1, -1         # Coordenadas X,Y finales del rectángulo
x1, y1 = -1, -1         # Coordenadas X,Y del primer punto (esquina superior izquierda)
x2, y2 = -1, -1         # Coordenadas X,Y del segundo punto (esquina superior derecha)
x3, y3 = -1, -1         # Coordenadas X,Y del tercer punto (esquina inferior izquierda)

def seleccionar(event, x, y, flags, param):         # Función callback para cuando se detecta un evento del mouse
    global drawing, waiting, selecting, xi, yi, xf, yf, x1, y1, x2, y2, x3, y3, img, img1  # Declaramos las variables globales para que no cree nuevas
    if waiting is False and selecting is False:        # Este if evita que se dibuje un nuevo rectangulo sin haber guardado o reseteado el anterior
        if event == cv2.EVENT_LBUTTONDOWN:      # Si se presiona el boton izquierdo
            drawing = True                      # Se activa el modo dibujo
            xi, yi = x, y                       # Y se guardan las coordenadas iniciales
        elif event == cv2.EVENT_MOUSEMOVE:      # Si se mueve el mouse
            if drawing is True:                 # Y se está dibujando
                img1 = np.copy(img)             # Carga nuevamente la imagen original (para que no queden todos los rectangulos anteriores)
                cv2.rectangle(img1, (xi, yi), (x, y), (0, 0, 255), 1)   # Y dibuja un rectangulo desde x,y inicial hasta el punto actual
        elif event == cv2.EVENT_LBUTTONUP:      # Si se suelta el boton del mouse
            drawing = False         # Salimos del modo dibujo
            waiting = True          # Entramos en el modo de espera
            xf, yf = x, y           # Guardamos las coordenadas finales

    if selecting is True:           # Si ya seleccionamos el rectangulo y presionamos la tecla "a"
        if event == cv2.EVENT_LBUTTONDOWN:  #Si se hace click izquierdo
            if (x1 == -1):            #Si no se selecciono el primer punto, guardamos las coordenadas (no verificamos el valor de "y1" porque no se puede cambiar solo el valor de x1)
                x1 = x
                y1 = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
            elif (x2 == -1) and ( (x1 != x) or (y1 != y) ):       #Si no se selecciono el segundo punto, y no se clickea 2 veces en el mismo lugar, guardamos las coordenadas
                x2 = x
                y2 = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
            elif (x3 == -1):        #Si no se selecciono el tercer punto
                if not( ( (x1 == x2) and (x1 == x) ) or ( (y1 == y2) and (y1 == y) ) ):  #Si los puntos son colineales se cumple que x1=x2=x3 o y1=y2=y3. Caso contrario no son colineales
                    x3 = x
                    y3 = y
                    cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
                    transf_afin()



def transf_afin():
    global xi, yi, xf, yf, x1, y1, x2, y2, x3, y3, selecting      # Declaramos las variables globales para que no cree nuevas

    selecting = False   # Salimos del modo de seleccion porque ya tenemos los 3 puntos

    if xi > xf:         # En esta porción de codigo se verifica que las coordenadas finales sean mayores que las iniciales
        aux = xi        # En caso contrario, se intercambian
        xi = xf         # Se hace por las dudas comiencen a dibujar el rectángulo desde abajo o desde la derecha
        xf = aux
    if yi > yf:
        aux = yi
        yi = yf
        yf = aux

    (h, w, d) = img1.shape   #Obtenemos el tamaño

    rec = np.zeros((h, w, d), np.uint8)     # Creamos una matriz del tamaño de la imagen original rellena con ceros

    for i in range(h):      # Iteramos la cantidad de filas que tenga la imagen
        for j in range(w):  # Iteramos la cantidad de columnas que tenga la imagen
            if (i >= yi and i <= yf) and (j >= xi and j <= xf): #Si el pixel está dentro del rectangulo seleccionado
                rec[i][j] = img[i][j]     # Guardamos cada pixel de la porción seleccionada en la matriz creada anteriormente

    puntos_orig = np.float32([[xi,yi], [xf,yi], [xi,yf]])   #Seleccionamos la esquina superior izquierda, la superior derecha y la inferior izquierda del rectangulo seleccionado como puntos originales

    puntos_transf = np.float32([[x1,y1], [x2,y2], [x3,y3]])

    M = cv2.getAffineTransform(puntos_orig, puntos_transf)

    img_transf = cv2.warpAffine(rec, M, (w, h))     #Aplicamos la transformacion

    for i in range(h):      # Iteramos la cantidad de filas que tenga la imagen
        for j in range(w):  # Iteramos la cantidad de columnas que tenga la imagen
            if ( img_transf[i][j][0] != 0 and img_transf[i][j][1] != 0 and img_transf[i][j][2] != 0 ): #Si el pixel transformado no es negro (correspondiente al fondo)
                img1[i][j] = img_transf[i][j]     # Guardamos cada pixel de la imagen transformada en la imagen original

    cv2.imwrite("Recorte_Transformado.jpg", img1)     # Guardamos esta nueva imagen como Recorte_Transformado.jpg

    x1, y1 = -1, -1         #Reseteamos coordenadas para poder volver a seleccionar
    x2, y2 = -1, -1
    x3, y3 = -1, -1  

img = cv2.imread("hojas.jpg")       # Leemos la imagen a recortar
img1 = np.copy(img)         # Creamos una segunda imagen que es la que vamos a manipular. Esto sirve para cargar la original cada vez que vayamos a dibujar y que no queden los dibujos anteriores
cv2.namedWindow('Image')    # Creamos una ventana llamada image
cv2.setMouseCallback('Image', seleccionar)  # Definimos que si se produce un evento de mouse sobre esa ventana se debe llamar a la función seleccionar

while(1):
    cv2.imshow('Image', img1)   # Mostramos la imagen en pantalla
    k = cv2.waitKey(1) & 0xFF   # Esperamos 1 ms a que se presione una tecla
    if k == ord('a') and waiting is True:   # Si la tecla que se presionó es una a, y ya selecionamos una porcion de la imagen
        img1 = np.copy(img)     # Cargamos nuevamente la imagen original
        waiting = False         # Salimos del modo de espera
        selecting = True        # Y activamos el modo de seleccion para elegir los 3 puntos
    elif k == ord('r'):         # Si la tecla que se presionó es una r
        img1 = np.copy(img)     # Cargamos nuevamente la imagen original
        waiting = False         # Y salimos del modo de espera para poder selecionar nuevamente
    elif k == ord('q'):         # Si la tecla que se presionó es una q
        break                   # Salimos

cv2.destroyAllWindows()         # Cerramos todas las ventanas abiertas
