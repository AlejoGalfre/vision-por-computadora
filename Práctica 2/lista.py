def imprimir_matriz(lista):
    for i in range(0, 4):
        for j in range(0, 4):
            print(lista[i][j], end = " ")
            if j == 3:
                print("")


fila1 = [2, 2, 5, 6]
fila2 = [0, 3, 7, 4]
fila3 = [8, 8, 5, 2]
fila4 = [1, 5, 6, 1]

lista = [fila1, fila2, fila3, fila4]

print("La matriz original es:\n")
imprimir_matriz(lista)

#Primer consigna
sub_array = lista[2]    #Pasamos por referencia ; equivalente a usar fila3
print("\n1 - Seleccionamos el sub-arreglo correspondiente a la 3ra fila:\n\n", sub_array)

#Segunda consigna
for i in range(0, 4):
    for j in range(0, 4):
        if i == j:
            lista[i][j] = 0

print("\n2 - Igualamos los elementos de la diagonal principal a 0:\n")
imprimir_matriz(lista)


#Tercera consigna
suma = 0
for i in range(0, 4):
    for j in range(0, 4):
        suma += lista[i][j]

print("\n3 - Calculamos la suma de todos los elementos de esta nueva matriz:\n")
print('Suma = ', suma)

#Cuarta consigna
for i in range(0, 4):
    for j in range(0, 4):
        if (lista[i][j] % 2) == 0:
            lista[i][j] = 0
        else:
            lista[i][j] = 1

print("\n4 - Igualamos los elementos pares a 0 y los impares a 1:\n")
imprimir_matriz(lista)
