import cv2              #Importamos libreria para leer imagen, obtener tamaño, transformarla y mostrarla
import numpy as np      #Importamos libreria para funciones trigonometricas y arreglos

def transf_euclid(angle = 0, tx = 0, ty = 0, s = 1.0):   #Rotación, traslacion y escalado a aplicar ; valores por defecto 0

    M = np.float32([ [s * np.cos(angle), s * np.sin(angle), tx], [-s * np.sin(angle), s * np.cos(angle), ty] ])  #Calculamos parte de la matriz H

    img = cv2.imread("hojas.jpg")   #Cargamos la imagen

    (h, w, d) = img.shape   #Obtenemos el tamaño

    img_transf = cv2.warpAffine(img, M, (w, h))     #Aplicamos la transformacion

    return img_transf   #Devolvemos la imagen transformada

tx = 300    #Traslacion en x
ty = 300    #Traslacion en y
angle = np.pi/4     #Ángulo de rotación
s = 0.5       #Factor de escala

imagen = transf_euclid(angle, tx, ty, s)

cv2.imshow("Imagen transformada", imagen)   #Mostramos la imagen transformada
cv2.waitKey(5000)       #Esperamos 5 segundos

cv2.destroyAllWindows()     #Cerramos todas las ventanas abiertas
