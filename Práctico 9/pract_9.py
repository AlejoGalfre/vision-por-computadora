import cv2              #Importamos libreria para leer imagen, obtener tamaño, transformarla y mostrarla
import numpy as np      #Importamos libreria para funciones trigonometricas y arreglos

selecting = True      # Esta variable indica que se están seleccionando los 2 puntos para la medición. Se pone en False luego de elegir 2 puntos validos y en True luego de tocar la r para resetear
xi, yi = -1, -1         # Coordenadas X,Y del primer punto (punto inicial)
xf, yf = -1, -1         # Coordenadas X,Y del segundo punto (punto final)

def seleccionar(event, x, y, flags, param):         # Función callback para cuando se detecta un evento del mouse
    global selecting, xi, yi, xf, yf, img_transf, img1  # Declaramos las variables globales para que no cree nuevas

    if selecting is True:           # Si todavia no elegimos los puntos
        if event == cv2.EVENT_LBUTTONDOWN:  #Si se hace click izquierdo
            if (xi == -1):            #Si no se selecciono el primer punto, guardamos las coordenadas (no verificamos el valor de "yi" porque no se puede cambiar solo el valor de xi)
                xi = x
                yi = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
            elif (xf == -1) and ( (xi != x) or (yi != y) ):       #Si no se selecciono el segundo punto, y no se clickea 2 veces en el mismo lugar, guardamos las coordenadas
                xf = x
                yf = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
                medir()

def medir():

    global xi, yi, xf, yf, selecting      # Declaramos las variables globales para que no cree nuevas

    selecting = False   # Salimos del modo de seleccion porque ya tenemos los 2 puntos

    cv2.line(img1, (xi, yi), (xf, yf), (255, 0, 0), 1) #Dibujamos una linea azul de ancho 1 entre ambos puntos

    distancia_pixeles = np.sqrt( (xf - xi)**2 + (yf - yi)**2 )

    distancia_m = distancia_pixeles / (2.4 * 100)   #2.4 es la equivalencia con los pixeles y 100 para pasar los cm a m

    cv2.putText(img1, str( round(distancia_m, 5) )+"m", (xi, yi), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    xi, yi = -1, -1         #Reseteamos coordenadas para poder volver a seleccionar
    xf, yf = -1, -1

#PROGRAMA PRINCIPAL
nombre_archivo = "puerta.jpg"
img = cv2.imread(nombre_archivo)   #Cargamos la imagen
(h, w, d) = img.shape   #Obtenemos el tamaño de la imagen

#La imagen original es de 3096 x 4128, asique le reducimos el tamaño
escala = 20/100     #Escala = 20%
w_reduc = int(w*escala)
h_reduc = int(h*escala)

img_reduc = cv2.resize(img, (w_reduc, h_reduc), interpolation = cv2.INTER_AREA) #Reducimos el tamaño conservando la relacion de aspecto

img_gris = cv2.cvtColor(img_reduc, cv2.COLOR_BGR2GRAY)    #Pasamos a escala de grises
img_gris = np.float32(img_gris)     #Modificamos el tipo de datos de los pixeles (necesario para la funcion de Harris)

fuente = img_gris   #Definimos los parametros para la funcion de Harris
tamaño_parche = 2
tamaño_kernel = 3
k = 0.156   #Para valores mayores de K, no se detecta la esquina inferior izquierda de la puerta

esquinas = cv2.cornerHarris(fuente, tamaño_parche, tamaño_kernel, k) #Detectamos las esquinas y guardamos sus coordenadas

umbral = 0.0006 * esquinas.max()   #Definimos un umbral igual al 0.06% del valor maximo detectado (Para valores mayores de umbral, no se detecta la esquina superior izquierda de la puerta)

#Filtramos la cantidad de esquinas detectadas al numero minimo que incluya a las de la puerta
for i in range(h_reduc):
    for j in range(w_reduc):
        if (esquinas[i][j] > umbral):
            esquinas[i][j] = 255
        else:
            esquinas[i][j] = 0

#cv2.imshow('Esquinas1', esquinas)

x1, y1 = 0, 0   #Primer punto a proyectar
x2, y2 = 0, 0   #Segundo punto a proyectar

contador = 0    #Contador de esquinas guardadas

for i in range(h_reduc):    #Recorremos la matriz de esquinas filtradas
    for j in range(w_reduc):
        if (esquinas[i][j] == 255): #Debido a la forma de la imagen y al resultado del detector de esquinas, la primera que encuentre sera la correspondiente a la esquina superior derecha
            contador = contador + 1

            if contador == 1:
                x1, y1 = j, i
            elif contador == 8:     #El octavo punto que encuentra, corresponde a la esquina superior izquierda
                x2, y2 = j, i
                break

    if x2 != 0:
        break

x3, y3 = 0, 0   #Tercer punto a proyectar
vecindad = 10
contador = 0

for i in range(y2+1, h_reduc):              #Recorremos la matriz de esquinas de forma vertical, partiendo desde la fila siguiente al segundo punto encontrado y tomando las 10 columnas vecinas (porque no están perfectamente alineados)
    for j in range(-vecindad, vecindad+1):
        if (esquinas[i][x2+j] == 255):
            contador = contador + 1

            if contador == 2:                   #La primera esquina que encuentra es la de un zocalo, la segunda es la de la puerta
                x3, y3 = x2+j, i                #Guardamos las coordenadas de la 3era esquina (inferior izquierda)
                break                           #Salimos del bucle

x4, y4 = 0, 0   #Cuarto punto a proyectar
vecindad = 30
contador = 0

for i in range(y3+1, h_reduc):              #Recorremos la matriz de esquinas de forma vertical, partiendo desde la fila siguiente al tercer punto encontrado y tomando las 10 columnas vecinas a la primera esquina encontrada (porque no están perfectamente alineados)
    for j in range(-vecindad, vecindad+1):
        if (esquinas[i][x1+j] == 255):
            contador = contador + 1

            if contador == 2:                   #El algoritmo detecta dos esquinas juntas, pero la segunda coincide mejor con la real
                x4, y4 = x1+j, i                #Guardamos las coordenadas de la 4ta esquina (inferior derecha)
                break                           #Salimos del bucle

alto_real = 195     #Altura de la puerta en cm
ancho_real = 185    #Ancho de la puerta en cm
escala_pixel = 2.4  #Se eligió esta escala porque la imagen sin rectificar mide aproximadamente 505 x 410 pixeles (para que la puerta mantenga mas o menos el mismo tamaño)

alto_pixeles = alto_real * escala_pixel     #Cantidad de pixeles de alto de la imagen transformada
ancho_pixeles = ancho_real * escala_pixel   #Cantidad de pixeles de ancho de la imagen transformada

puntos_orig = np.float32([ [x1, y1], [x2, y2], [x3, y3], [x4, y4] ])  #Los puntos originales son las 4 esquinas de la puerta en la imagen sin rectificar
puntos_transf = np.float32([ [x1, y1], [x1 - ancho_pixeles, y1], [x1 - ancho_pixeles, y1 + alto_pixeles], [x1,  y1 + alto_pixeles] ])    #Vamos a tomar como punto de "referencia" el x1,y1 y dibujar el rectangulo desde ahi

M = cv2.getPerspectiveTransform(puntos_orig, puntos_transf)

img_transf = cv2.warpPerspective(img_reduc, M, (w_reduc, h_reduc))     #Aplicamos la transformacion

#print("x1, y1:", x1, y1)
#print("x2, y2:", x2, y2)
#print("x3, y3:", x3, y3)
#print("x4, y4:", x4, y4)

img_esq = np.copy(img_reduc) #Creamos una nueva imagen para marcar las esquinas

img_esq[y1][x1] = [0, 0, 255] #Pintamos de rojo los pixeles correspondientes a las esquinas
img_esq[y2][x2] = [0, 0, 255] #Pintamos de rojo los pixeles correspondientes a las esquinas
img_esq[y3][x3] = [0, 0, 255] #Pintamos de rojo los pixeles correspondientes a las esquinas
img_esq[y4][x4] = [0, 0, 255] #Pintamos de rojo los pixeles correspondientes a las esquinas

cv2.imshow('Fachada original (con esquinas detectadas)', img_esq)              #Mostramos la imagen

img1 = np.copy(img_transf)  # Creamos una segunda imagen que es la que vamos a manipular. Esto sirve para cargar la original cada vez que vayamos a dibujar y que no queden los dibujos anteriores
cv2.namedWindow('Fachada rectificada')    # Creamos una ventana llamada Fachada rectificada
cv2.setMouseCallback('Fachada rectificada', seleccionar)  # Definimos que si se produce un evento de mouse sobre esa ventana se debe llamar a la función seleccionar

while(1):
    cv2.imshow('Fachada rectificada', img1)      #Mostramos la imagen rectificada
    c = cv2.waitKey(1) & 0xFF   # Esperamos 1 ms a que se presione una tecla
    if c == ord('r'):         # Si la tecla que se presionó es una r
        img1 = np.copy(img_transf)     # Cargamos nuevamente la imagen original
        selecting = True
    elif c == ord('q'):         # Si la tecla que se presionó es una q
        break                   # Salimos

cv2.destroyAllWindows()         # Cerramos todas las ventanas abiertas
