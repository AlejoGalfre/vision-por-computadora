import cv2  #Importamos la libreria donde se encuentran lasfunciones para leer y guardar imágenes

img = cv2.imread("hojas.jpg", 0)    #Leemos la imagen hojas.jpg y guardamos sus pixeles en img ; El argumento 0 es para que la imagen se cargue en escala de grises (blanco y negro)

filas = len(img)
columnas = len(img[0])

for fila in range(0, filas):
    for columna in range(0, columnas):        #Recorremos cada pixel de la imagen
        if img[fila][columna] < 230:   #Si el pixel no es lo suficientemente blanco (correspondiente al fondo)
            img[fila][columna] = 0      #Hacemos que el pixel sea negro (de tal forma que todas las hojas queden negras)

cv2.imwrite("Resultado.jpg", img)
