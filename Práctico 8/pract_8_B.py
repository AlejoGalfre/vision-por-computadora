import cv2              # Importamos la libreria para leer y escribir imágenes, y detectar los eventos del mouse
import numpy as np      # Importamos la libreria para crear y copiar matrices y la renombramos

#drawing = False         # Esta variable indica que se está dibujando el rectangulo. Se pone en True cuando se apreta el mouse y en False cuando se suelta
#waiting = False         # Esta variable indica que ya se seleccionó parte de la imagen y se está esperando a que se presione una tecla. Se pone en true al terminar de dibujar el rectangulo y en False al guardar o resetear la selección
selecting = False       # Esta variable indica que se están seleccionando los 3 puntos para la transformacion afin. Se pone en True luego de presionar la letra a y en False luego de elegir 3 puntos validos

x1, y1 = -1, -1         # Coordenadas X,Y del primer punto (esquina superior izquierda)
x2, y2 = -1, -1         # Coordenadas X,Y del segundo punto (esquina superior derecha)
x3, y3 = -1, -1         # Coordenadas X,Y del tercer punto (esquina inferior izquierda)
x4, y4 = -1, -1         # Coordenadas X,Y del cuarto punto (esquina inferior derecha)

def seleccionar(event, x, y, flags, param):         # Función callback para cuando se detecta un evento del mouse
    global selecting, x1, y1, x2, y2, x3, y3, x4, y4, img, img1  # Declaramos las variables globales para que no cree nuevas

    if selecting is True:           # Si ya seleccionamos el rectangulo y presionamos la tecla "h"
        if event == cv2.EVENT_LBUTTONDOWN:  #Si se hace click izquierdo
            if (x1 == -1):            #Si no se selecciono el primer punto, guardamos las coordenadas (no verificamos el valor de "y1" porque no se puede cambiar solo el valor de x1)
                x1 = x
                y1 = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
            elif (x2 == -1) and ( (x1 != x) or (y1 != y) ):       #Si no se selecciono el segundo punto, y no se clickea 2 veces en el mismo lugar, guardamos las coordenadas
                x2 = x
                y2 = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
            elif (x3 == -1) and ( (x1 != x) or (y1 != y) ) and ( (x2 != x) or (y2 != y) ):       #Si no se selecciono el tercer punto, y no se clickea 2 veces en el mismo lugar, guardamos las coordenadas
                x3 = x
                y3 = y
                cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
            elif (x4 == -1):        #Si no se selecciono el tercer punto
                if not( ( (x1 == x2) and (x3 == x) and (x1 == x3) ) or ( (y1 == y2) and (y3 == y) and (y1 == y3) ) ):  #Si los puntos son colineales se cumple que x1=x2=x3=x4 o y1=y2=y3=y4. Caso contrario no son colineales
                    x4 = x
                    y4 = y
                    cv2.circle(img1, (x, y), 1, (0, 0, 255))     #Dibujamos un punto rojo para mostrar la seleccion
                    homograf()



def homograf():
    global x1, y1, x2, y2, x3, y3, x4, y4, selecting      # Declaramos las variables globales para que no cree nuevas

    selecting = False   # Salimos del modo de seleccion porque ya tenemos los 4 puntos

    (h, w, d) = img1.shape   #Obtenemos el tamaño

    puntos_orig = np.float32([[x1,y1], [x2,y2], [x3,y3], [x4,y4]])   #Los 4 puntos seleccionados por el usuario son las esquinas de la porción a rectificar

    puntos_transf = np.float32([[0,0], [w-1,0], [0,h-1], [w-1,h-1]]) #Se eligen como puntos transformados las esquinas de la imagen original para obtener una imagen rectangular de ese tamaño

    M = cv2.getPerspectiveTransform(puntos_orig, puntos_transf)

    img_transf = cv2.warpPerspective(img1, M, (w, h))     #Aplicamos la transformacion

    cv2.imwrite("Recorte_Transformado.jpg", img_transf)     # Guardamos esta nueva imagen como Recorte_Transformado.jpg

    x1, y1 = -1, -1         #Reseteamos coordenadas para poder volver a seleccionar
    x2, y2 = -1, -1
    x3, y3 = -1, -1
    x4, y4 = -1, -1

img = cv2.imread("imagen1.jpeg")       # Leemos la imagen a recortar
img1 = np.copy(img)         # Creamos una segunda imagen que es la que vamos a manipular. Esto sirve para cargar la original cada vez que vayamos a dibujar y que no queden los dibujos anteriores
cv2.namedWindow('Image')    # Creamos una ventana llamada image
cv2.setMouseCallback('Image', seleccionar)  # Definimos que si se produce un evento de mouse sobre esa ventana se debe llamar a la función seleccionar

while(1):
    cv2.imshow('Image', img1)   # Mostramos la imagen en pantalla
    k = cv2.waitKey(1) & 0xFF   # Esperamos 1 ms a que se presione una tecla
    if k == ord('h'):   # Si la tecla que se presionó es una h
        img1 = np.copy(img)     # Cargamos nuevamente la imagen original
        selecting = True        # Y activamos el modo de seleccion para elegir los 4 puntos
    elif k == ord('r'):         # Si la tecla que se presionó es una r
        img1 = np.copy(img)     # Cargamos nuevamente la imagen original
    elif k == ord('q'):         # Si la tecla que se presionó es una q
        break                   # Salimos

cv2.destroyAllWindows()         # Cerramos todas las ventanas abiertas
