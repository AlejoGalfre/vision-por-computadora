import cv2              #Importamos libreria para leer imagen, obtener tamaño, transformarla y mostrarla
import numpy as np      #Importamos libreria para funciones trigonometricas y arreglos

def homograf(img, puntos_orig, puntos_transf):

    (h, w, d) = img.shape   #Obtenemos el tamaño

    M = cv2.getPerspectiveTransform(puntos_orig, puntos_transf)

    img_transf = cv2.warpPerspective(img, M, (w, h))     #Aplicamos la transformacion

    return img_transf   #Devolvemos la imagen transformada

img = cv2.imread("hojas.jpg")   #Cargamos la imagen
(h, w, d) = img.shape   #Obtenemos el tamaño

#En particular para este práctico, se decidió arbitrariamente que las esquinas del lado izquierdo se desplacen hasta la mitad de la imagen y se acerquen al centro de la misma, mientras que las esquinas derechas permanecen en su lugar

puntos_orig = np.float32([[0,0], [w-1,0], [0,h-1], [w-1,h-1]]) #Se eligen como puntos originales la esquina superior izquierda, la esquina superior derecha, la esquina inferior izquierda y la esquina inferior derecha

puntos_transf = np.float32([[round(w/2),round(1/4*h)], [w-1,0], [round(w/2),round(3/4*h)], [w-1,h-1]]) #Las 2 esquinas derechas permanecen en su lugar. Las esquinas izquierdas se trasladan hasta la mitad de la imagen en coordenadas de X, y 1/4 y 3/4 del alto de la imagen en coordenadas Y, acercandose hacia el centro

imagen = homograf(img, puntos_orig, puntos_transf)

cv2.imshow("Imagen transformada", imagen)   #Mostramos la imagen transformada
cv2.waitKey(5000)       #Esperamos 5 segundos

cv2.destroyAllWindows()     #Cerramos todas las ventanas abiertas
